## REST

Для запуска REST-версии необходимо набрать:

```bash
cd monkeyquiz-rest
npm i
npm start -p 4000
```

Для запуска REST-клиента:

```bash
cd monkeyquiz-rest-client
npm i
npm start
```


## GraphQL

Для запуска GraphQL API:

```bash
cd monkeyquiz-api
npm i
npm run build
npm start -p 4000
```

Для запуска GraphQL клиента:

```bash
cd monkeyquiz
npm i
npm start
```

## Данные авторизации

```
email: admin@example.com
password: 12345
```