const express = require('express')
const { ApolloServer, gql } = require('apollo-server-express')
const { MongoClient, ObjectId } = require('mongodb')
let nodemailer = require('nodemailer')

const url = 'mongodb://admin:s012345678@ds331198.mlab.com:31198/monkeyquiz'
const dbName = "monkeyquiz"

const mongoClient = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

// Create a SMTP transport object
let transport = nodemailer.createTransport({
  service: 'Hotmail',
  auth: {
    user: "vmp@live.ru",
    pass: "aIw*$6W8*x"
  },
  tls: {
    rejectUnauthorized: false
  }
})

const typeDefs = gql`
  enum Currency {
    RUB
    USD
    EUR
  }

  enum SortBy {
    CITY
    DATE
    GAME_TYPE
  }

  type User {
    username: String
    password: String
  }

  type Quiz {
    _id: ID
    title: String
    location: String
    gameType: String
    date: String
    time: String
    city: String
    fullDescription: String
    photo: String
    cost: Float
    currency: Currency
  }

  input CreateQuizInput {
    title: String
    location: String
    gameType: String
    date: String
    time: String
    city: String
    fullDescription: String
    photo: String
    cost: Float
    currency: Currency
  }

  type Command {
    _id: ID
    name: String
    city: String
    score: Int
  }

  input CommandInput {
    name: String
    city: String
    score: Int
  }

  enum SaveResult {
    SUCCESS
    ERROR
  }

  type Query {
    quizes(sortby: SortBy, limit: Int): [Quiz]
    quiz(id: ID!): Quiz

    commands: [Command]
    command(id: ID!): Command

    gameTypes: [String]
    cities: [String]
  }

  type Mutation {
    buy(id: ID!): Quiz

    createQuiz(input: CreateQuizInput): SaveResult
    editQuiz(id: ID!, input: CreateQuizInput): SaveResult
    deleteQuiz(id: ID!): SaveResult

    editCommand(id: ID!, input: CommandInput): SaveResult
    createCommand(input: CommandInput): SaveResult
    deleteCommand(id: ID!): SaveResult
  }
`

const resolvers = {
  Query: {
    quizes: async (parent, args, { mongoClient }, info) => {
      let s = null
      let l = args.limit ? args.limit : 0

      if (args.sortby) {
        if (args.sortby == "CITY")        s = { city: 1 }
        if (args.sortby == "GAME_TYPE")   s = { gameType: 1 }
        if (args.sortby == "DATE")        s = { date: -1 }
      }

      let qs = await mongoClient.db(dbName)
        .collection("quizes")
        .find()
        .sort(s)
        .limit(l)
        .toArray()

      return qs
    },

    quiz: async (parent, { id }, { mongoClient }, info) => {
      let q = await mongoClient.db(dbName)
        .collection('quizes')
        .findOne({ _id: +id })

      return q
    },

    commands: async (parent, args, { mongoClient }, info) => {
      let cm = await mongoClient.db(dbName)
        .collection('commands')
        .find()
        .sort({ score: -1 })
        .toArray()
      
      return cm
    },

    command: async (parent, { id }, { mongoClient }, info) => {
      let cm = await mongoClient.db(dbName)
        .collection('commands')
        .findOne({ _id: ObjectId(id) })

      return cm
    },

    gameTypes: async (parent, args, { mongoClient }, info) => {
      let g = await mongoClient.db(dbName)
        .collection('quizes')
        .distinct('gameType')

      return g
    },

    cities: async (parent, args, { mongoClient }, info) => {
      let c = await mongoClient.db(dbName)
        .collection('quizes')
        .distinct('city')

      return c
    }
  },

  Mutation: {
    buy: async (parent, { id }, { mongoClient }, info) => {

      const qz = await mongoClient.db(dbName)
        .collection('quizes')
        .findOne({ _id: +id })

      console.log(qz)

      // let message = {
      //   from: 'vmp@live.ru',
      //   to: 'vmp@live.ru',
      //   subject: 'Письмо',
      //   text: JSON.stringify(qz),
      //   // html: ''
      // }

      // transport.sendMail(message, (err) => {
      //   if (err)
      //     console.log(err)
      //   console.log('Message sended succefully...')
      // })

      return qz
    },

    createQuiz: async (paren, { input }, { mongoClient }, info) => {

      let ids = await mongoClient.db(dbName)
        .collection('quizes')
        .distinct("_id")
      
      const id = ids.sort((a, b) => {
        if (a > b) return -1
        if (a = b) return 0
        if (a < b) return 1
      })

      let qz = await mongoClient.db(dbName)
        .collection('quizes')
        .insertOne({ _id: id[0] + 1, ...input })
      
      if (qz && qz.result.ok) {
        return "SUCCESS"
      }
  
      return "ERROR"
    },

    editQuiz: async (parent, { id, input }, { mongoClient }, info) => {
      let qz = await mongoClient.db(dbName)
        .collection('quizes')
        .findOneAndUpdate({ _id: +id }, { $set: { ...input } })
      
      if (qz && qz.ok) {
        return "SUCCESS"
      }
      
      return "ERROR"
    },

    deleteQuiz: async (parent, { id }, {mongoClient }, info) => {
      let qz = await mongoClient.db(dbName)
        .collection('quizes')
        .deleteOne({ _id: +id })
        
      if (qz && qz.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    },

    editCommand: async (parent, { id, input }, { mongoClient }, info) => {
      let cm = await mongoClient.db(dbName)
        .collection('commands')
        .findOneAndUpdate({ _id: ObjectId(id) }, { $set: { ...input } })
      
      if (cm && cm.ok) {
        return "SUCCESS"
      }
      
      return "ERROR"
    },

    createCommand: async (parent, { input }, {mongoClient }, info) => {
      let cm = await mongoClient.db(dbName)
        .collection('commands')
        .insertOne({ ...input })
        
      if (cm && cm.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    },

    deleteCommand: async (parent, { id }, {mongoClient }, info) => {
      let cm = await mongoClient.db(dbName)
        .collection('commands')
        .deleteOne({ _id: ObjectId(id) })
        
      if (cm && cm.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    }
  }
}


const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: {
    mongoClient
  }
})


mongoClient.connect().then(() => {
  const app = express()

  server.applyMiddleware({ app })

  app.listen({ port: 4000 }, () =>
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
  )
}).catch(err => console.log(err))
