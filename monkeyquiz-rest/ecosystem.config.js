module.exports = {
  apps: [
    {
      name: "monkeyquiz-rest",
      script: "./server.js",
      cwd: "./",
      watch: true,
      env: {
        NODE_ENV: "development",
        PORT: 4000,
        JWT_SECRET: "SUPER SECRET",

        DB_HOST: "ds331198.mlab.com",
        DB_PORT: 31198,
        DB_USER: "admin",
        DB_PASS: "s012345678",
        DB_NAME: "monkeyquiz",
        BASE_URL: "https://localhost:4000/api"
      }
    }
  ]
};
