module.exports = {
  NODE_ENV: process.env.NODE_ENV || "development",
  PORT: process.env.PORT || 4000,
  JWT_SECRET: process.env.JWT_SECRET || "SUPER SECRET",

  DB_HOST: process.env.DB_HOST || "ds331198.mlab.com",
  DB_PORT: process.env.DB_PORT || 31198,
  DB_USER: process.env.DB_USER || "admin",
  DB_PASS: process.env.DB_PASS || "s012345678",
  DB_NAME: process.env.DB_NAME || "monkeyquiz",
  BASE_URL: process.env.BASE_URL || "https://localhost:4000/api"
}