const jwt = require('jsonwebtoken')
const config = require('../config')

const auth = async (req, res, next) => {
  const authHeader = req.headers.authorization || req.headers["Authorization"]
  if (!authHeader) {
    req.isAuth = false
    return next()
  }
  
  const token = authHeader.trim().split(" ")[1]
  if (!token || token === "") {
    req.isAuth = false
    return next()
  }
  
  let decodedToken
  try {
    decodedToken = await jwt.verify(token, config.JWT_SECRET)
  } catch (error) {
    req.isAuth = false
    return next()
  }

  req.isAuth = true
  req.userId = decodedToken.userId
  next()
}

module.exports = auth