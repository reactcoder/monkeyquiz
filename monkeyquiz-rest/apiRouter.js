const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const { ObjectId } = require('mongodb')
const config = require('./config')
const { connectDB } = require('./lib/mongo')

const router = express.Router()

/**
 * Get all quizes
 */
router.get('/quizes', async (req, res, next) => {
  try {
    const db = await connectDB()

    let s = null
    let l = req.query.limit ? +req.query.limit : 0
  
    if (req.query.sortby) {
      if (req.query.sortby == "CITY")        s = { city: 1 }
      if (req.query.sortby == "GAME_TYPE")   s = { gameType: 1 }
      if (req.query.sortby == "DATE")        s = { date: -1 }
    }

    const quizes = await db
      .collection('quizes')
      .find()
      .sort(s)
      .limit(l)
      .toArray()
    
    return res.status(200).json(quizes)
  } catch (error) {
    next(error)
  }
})

/**
 * Create new quiz
 */
router.post('/quizes', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    const ids = await db
      .collection('quizes')
      .distinct("_id")

    const sortedIds = ids.sort((a, b) => b - a)

    await db
      .collection('quizes')
      .insertOne({ _id: sortedIds[0] + 1, ...req.body })

    res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Get quiz
 */
router.get('/quizes/:id', async (req, res, next) => {
  try {
    const db = await connectDB()

    const quiz = await db
      .collection('quizes').findOne({ _id: +req.params.id })
    
    if (!quiz)
      throw new Error("Quiz not found!")

    res.status(200).json(quiz)
  } catch (error) {
    next(error)
  }
})

/**
 * Update quiz
 */
router.put('/quizes/:id', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    await db
      .collection('quiz')
      .findOneAndUpdate({ _id: +req.params.id }, { $set: { ...req.body } })

    return res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Delete quiz
 */
router.delete('/quizes/:id', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    await db
      .collection('quizes')
      .deleteOne({ _id: +req.params.id })
    
    return res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Get all commands
 */
router.get('/commands', async (req, res, next) => {
  try {
    const db = await connectDB()

    const commands = await db
      .collection('commands')
      .find()
      .sort({ score: -1 })
      .toArray()
    
    return res.status(200).json(commands)
  } catch (error) {
    next(error)
  }
})

/**
 * Get command
 */
router.get('/commands/:id', async (req, res, next) => {
  try {
    const db = await connectDB()

    const command = await db
      .collection('commands')
      .findOne({ _id: ObjectId(req.params.id) })
    
    if (!command)
      throw new Error("Command not found!")

    res.status(200).json(command)
  } catch (error) {
    next(error)
  }
})

/**
 * Add new command
 */
router.post('/commands', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    await db
      .collection('commands')
      .insertOne({ ...req.body })

    res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Update command
 */
router.put('/commands/:id', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    await db
      .collection('commands')
      .findOneAndUpdate({ _id: ObjectId(req.params.id) }, { $set: { ...req.body } })

    return res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Delete command
 */
router.delete('/commands/:id', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()

    await db
      .collection('commands')
      .deleteOne({ _id: ObjectId(req.params.id) })
    
    return res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Create user
 */
router.post('/create', async (req, res, next) => {
  try {
    const email = req.body.email ? req.body.email : ""
    const password = req.body.password ? req.body.password : ""

    if (email == "" || password == "") {
      throw new Error('Email and password could not be empty!')
    }

    const db = await connectDB()

    const user = await db
      .collection("users")
      .findOne({ email })

    if (user) {
      throw new Error("Пользователь с таким email уже существует!")
    }

    const hashedPassword = await bcrypt.hash(password, 12)

    await db
      .collection("users")
      .insertOne({ email, password: hashedPassword })
    
    return res.status(200).json({ result: "SUCCESS" })
  } catch (error) {
    next(error)
  }
})

/**
 * Login user
 */
router.post('/login', async (req, res, next) => {
  try {
    const { email, password } = req.body

    console.log(email, password)

    if (!email || !password) {
      throw new Error("Email and password could not be empty!")
    }

    const db = await connectDB()
    const user = await db
      .collection('users')
      .findOne({ email })

    if (!user) {
      throw new Error("User not found in system!")
    }

    const isEqual = await bcrypt.compare(password, user.password)
    if (!isEqual) {
      throw new Error("Password wrong!")
    }

    const payload = { userId: user._id }
    const token = jwt.sign(payload, config.JWT_SECRET, {
      expiresIn: '1h'
    })

    return res.status(200).json({ token })
  } catch (error) {
    next(error)
  }
})

/**
 * Game types
 */
router.get('/gameTypes', async (req, res, next) => {
  try {
    const db = await connectDB()

    const gameTypes = await db
      .collection('quizes')
      .distinct('gameType')
    
    return res.status(200).json(gameTypes)
  } catch (error) {
    next(error)
  }
})

/**
 * Cities list
 */
router.get('/cities', async (req, res, next) => {
  try {
    const db = await connectDB()

    const gameTypes = await db
      .collection('quizes')
      .distinct('city')
    
    return res.status(200).json(gameTypes)
  } catch (error) {
    next(error)
  }
})

/**
 * Current user
 */
router.get('/viewer', async (req, res, next) => {
  try {
    const { isAuth, userId } = req

    if (!isAuth) {
      return res.status(200).json({ isLoggedIn: false })
    }

    const db = await connectDB()
    const user = await db
      .collection('users')
      .findOne({ _id: ObjectId(userId) })

    return res.status(200).json({
      ...user,
      isLoggedIn: true,
      password: ""
    })
  } catch (error) {
    next(error)
  }
})

/**
 * Buy quiz
 */
router.post('/buy', async (req, res, next) => {
  try {
    const { isAuth } = req
    if (!isAuth) {
      throw new Error("Пользователь неавторизован!")
    }

    const db = await connectDB()
    const quiz = await db
      .collection('quizes')
      .findOne({ _id: +req.body.id })
    
    // let message = {
    //   from: 'vmp@live.ru',
    //   to: 'vmp@live.ru',
    //   subject: 'Письмо',
    //   text: JSON.stringify(qz),
    //   // html: ''
    // }

    // transport.sendMail(message, (err) => {
    //   if (err)
    //     console.log(err)
    //   console.log('Message sended succefully...')
    // })

    return res.status(200).json(quiz)
  } catch (error) {
    next(error)
  }
})

module.exports = router