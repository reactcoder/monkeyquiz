const express = require('express')
const cors = require('cors')
const config = require('./config')
const auth = require('./middleware/auth')
const apiRouter = require('./apiRouter')

const app = express()

app.use(cors())
app.use(express.json())

app.use(auth)

app.use('/api', apiRouter)

app.get('/debug', (req, res) => {
  res.json({
    NODE_ENV: config.NODE_ENV,
    PORT: config.PORT,
    BASE_URL: config.BASE_URL,
    DB_URI: config.DB_URI,
    JWT_SECRET: config.JWT_SECRET,
    DB_HOST: config.DB_HOST,
    DB_PORT: config.DB_PORT,
    DB_USER: config.DB_USER,
    DB_PASS: config.DB_PASS,
    DB_NAME: config.DB_NAME,
    BASE_URL: config.BASE_URL
  })
})

app.use(function(err, req, res, next) {
  res.status(200).json({ err: err.message })
})

const port = config.PORT

app.listen(port, (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log(`API started successfully on port ${port}`)
  }
})