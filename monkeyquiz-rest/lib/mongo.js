const { MongoClient } = require('mongodb')
const config = require('../config')

const DB_HOST = config.DB_HOST
const DB_PORT = config.DB_PORT
const DB_USER = config.DB_USER
const DB_PASS = config.DB_PASS
const DB_NAME = config.DB_NAME
const DB_URL = `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`

const DB_OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

const connectDB = async () => {
  const client = new MongoClient(DB_URL, DB_OPTIONS)

  if (!client.isConnected()) {
    await client.connect()
  }

  return client.db(DB_NAME)
}

module.exports = {
  connectDB
}