import React, { useRef, useState } from 'react'
import gql from 'graphql-tag'
import { Row, Input, Button, LoginForm, LoginLabel } from '../components/Global'
import { useMutation, useQuery } from '@apollo/react-hooks'
import Spinner, { MiniSpinner } from '../components/Spinner'

const USER_LOGIN = gql`
  mutation UserLogin($email: String!, $password: String!) {
    userLogin(email: $email, password: $password) {
      token
    }
  }
`

const GET_VIEWER = gql`
  query GetViewer {
    viewer {
      email
      isLoggedIn
    }
  }
`

function LoginPage(props) {
  const emailRef = useRef()
  const passwRef = useRef()

  const [userLogin, { loading: loginLoading, error: loginError }] = useMutation(USER_LOGIN)
  const { client, data, loading } = useQuery(GET_VIEWER)

  if (loading) return <Spinner />

  if (data.viewer.isLoggedIn) {
    props.history.push('/profile')
  }

  const handleOnClick = async () => {
    const email = emailRef.current.value
    const password = passwRef.current.value

    try {
      const { data } = await userLogin({ variables: { email, password } })

      if (data && data.userLogin) {
        localStorage.setItem('token', data.userLogin.token)
        props.history.push(`/profile`)
        await client.resetStore()
      }

    } catch (error) {
      console.log(error)
    }
  }

  return (
    <LoginForm>
      <Row>
        <h1>Login Form</h1>
      </Row>

      <Row small>
        <LoginLabel>
          Login:
          <Input type="text" placeholder="Login" ref={emailRef} />
        </LoginLabel>
      </Row>

      <Row small>
        <LoginLabel>
          Password:
          <Input type="text" placeholder="Password" ref={passwRef} />
        </LoginLabel>
      </Row>

      <Row style={{ justifyContent: "space-between", alignItems: "center" }}>
        <Button onClick={handleOnClick}>Login</Button>
        {loginLoading && <MiniSpinner />}
      </Row>

      {loginError && <Row>{loginError.graphQLErrors[0].message}</Row>}
    </LoginForm>
  )
}

export default LoginPage