import React, { useState } from 'react'
import QuizBlock from '../components/QuizBlock'
import EditQuizForm from '../components/EditQuizForm'

import { useQuery, useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import {
  Container,
  QuizWrapper,
  QuizHead,
  QuizContent,
  AddBlock,
  AddBlockInner
} from '../components/Global'
import Spinner from '../components/Spinner'

const GET_QUIZES = gql`
  query GetQuizes {
    viewer {
      email
      isLoggedIn
    }  
    quizes {
      _id
      title
      city
      location
      gameType
      date
      time
      cost
      currency
      photo
      fullDescription
    }
  }
`

const CREATE_QUIZ = gql`
  mutation CreateQuiz($input: CreateQuizInput) {
    createQuiz(input: $input)
  }
`

const EDIT_QUIZ = gql`
  mutation EditQuiz($id: ID!, $input: CreateQuizInput) {
    editQuiz(id: $id, input: $input)
  }
`

const DELETE_QUIZ = gql`
  mutation DeleteQuiz($id: ID!) {
    deleteQuiz(id: $id)
  }
`

function IndexPage() {
  const [isModalShow, setIsModalShow] = useState(false)
  const [quiz, setQuiz] = useState(null)

  const [createQuiz] = useMutation(CREATE_QUIZ)
  const [editQuiz] = useMutation(EDIT_QUIZ)
  const [deleteQuiz] = useMutation(DELETE_QUIZ)

  const { data, loading } = useQuery(GET_QUIZES)

  if (loading) return <Spinner />

  const { quizes, viewer } = data

  const handleSave = async (id, input) => {

    console.log('>>>handleSave', id, input)

    let result

    if (!id) {
      const { data } = await createQuiz({ variables: { input }, refetchQueries: [{ query: GET_QUIZES }] })

      console.log(data)

      if (data.createQuiz == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    } else {
      const { data } = await editQuiz({ variables: { id, input }, refetchQueries: [{ query: GET_QUIZES }] })

      if (data.editQuiz == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    }

    return result
  }

  const handleEdit = async (qz) => {
    setQuiz(qz)
    setIsModalShow(true)
  }

  const handleCreate = async () => {
    setQuiz(null)
    setIsModalShow(true)
  }

  const handleDelete = async (id) => {
    deleteQuiz({ variables: { id }, refetchQueries: [{ query: GET_QUIZES }] })
  }

  return (
    <QuizWrapper>
      <Container>
        <QuizHead>All Quizes</QuizHead>
        <QuizContent>
          { quizes.map(q =>
            <QuizBlock
              key={q._id}
              quiz={q}
              onEdit={viewer.isLoggedIn ? handleEdit : null}
              onDelete={viewer.isLoggedIn ? handleDelete : null}
            />)
          }
          {viewer.isLoggedIn &&
            <AddBlock onClick={handleCreate}>
              <AddBlockInner>+</AddBlockInner>
            </AddBlock>
          }
        </QuizContent>
      </Container>

      

      {isModalShow && <EditQuizForm quiz={quiz} onSave={handleSave} onClose={() => setIsModalShow(false)} />}
    </QuizWrapper>
  )
}

export default IndexPage