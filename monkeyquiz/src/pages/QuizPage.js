import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import RegisterForm from '../components/RegisterForm'
import {
  Button,
  Row,
  Icon,
  Clock,
  Compass,
  User,
  Price,
  QuizInfo,
  QuizHeader,
  QuizCity,
  QuizFooter,
  QuizLocation,
  QuizImg
} from '../components/Global'
import { convertCurrency } from '../utils/utils'
import Spinner from '../components/Spinner'

const GET_QUIZ = gql`
  query GetQuiz($id: ID!) {
    quiz(id: $id) {
      _id
      title
      city
      location
      gameType
      date
      cost
      currency
      photo
      fullDescription
    }
  }
`

function QuizPage({ match }) {
  const { id } = match.params
  const [isShow, setIsShow] = useState(false)
  const { data, loading } = useQuery(GET_QUIZ, { variables: { id: +id } })

  if (loading) return <Spinner />

  return (
    <QuizInfo>
      {isShow && <RegisterForm quiz={data.quiz} onClose={() => setIsShow(false)} />}

      <QuizImg src={data.quiz.photo}/>

      <QuizHeader>{data.quiz.title}</QuizHeader>
      <QuizCity>{data.quiz.city}</QuizCity>

      <Row>
        <Icon>
          <Clock />
        </Icon>
        <span><strong>Time:</strong> {new Date(data.quiz.date).toLocaleString()}</span>
      </Row>

      <Row small>
        <Icon>
          <Compass />
        </Icon>
        <QuizLocation><strong>Location:</strong> {data.quiz.location}</QuizLocation>
      </Row>

      <Row small>
        <Icon>
          <User />
        </Icon>
        <QuizLocation><strong>Game type:</strong> {data.quiz.gameType}</QuizLocation>
      </Row>

      <Row small>
        <Icon>
          <Price />
        </Icon>
        <QuizLocation><strong>Price:</strong> {convertCurrency(data.quiz.currency, data.quiz.cost)}</QuizLocation>
      </Row>

      <Row small>
        {data.quiz.fullDescription &&  <div>{data.quiz.fullDescription}</div>}
      </Row>

      <QuizFooter>
        <Button onClick={() => setIsShow(true)}>Registration</Button>
      </QuizFooter>
    </QuizInfo>
  )
}

export default QuizPage