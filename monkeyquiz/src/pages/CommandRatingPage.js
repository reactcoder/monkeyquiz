import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Section, Container, Title, Button, Row, TableWrapper, Table, Th } from '../components/Global'
import EditCommandForm from '../components/EditCommandForm'
import Spinner from '../components/Spinner'

const GET_COMMAND_LIST = gql`
  query GetCommandList {
    viewer {
      isLoggedIn
    } 
    commands {
      _id
      name
      city
      score
    }
  }
`

const CREATE_COMMAND = gql`
  mutation CreateCommand($input: CommandInput) {
    createCommand(input: $input)
  }
`

const EDIT_COMMAND = gql`
  mutation EditCommand($id: ID!, $input: CommandInput) {
    editCommand(id: $id, input: $input)
  }
`

const DELETE_COMMAND = gql`
  mutation DeleteCommand($id: ID!) {
    deleteCommand(id: $id)
  }
`

function CommandRatingPage() {
  const [isModalShow, setIsModalShow] = useState(false)

  const [cm, setCommand] = useState(null)

  const [deleteCommand] = useMutation(DELETE_COMMAND)
  const [createCommand] = useMutation(CREATE_COMMAND)
  const [editCommand] = useMutation(EDIT_COMMAND)

  const { data, loading } = useQuery(GET_COMMAND_LIST)

  if (loading) return <Spinner />

  const { commands, viewer } = data

  const handleEdit = (cm) => {
    setCommand(cm)
    setIsModalShow(true)
  }

  const handleCreate = () => {
    setCommand(null)
    setIsModalShow(true)
  }

  const handleDelete = (id) => {
    deleteCommand({ variables: { id }, refetchQueries: [{ query: GET_COMMAND_LIST }] })
  }

  const handleSave = async (id, input) => {
    let result

    if (!cm) {
      const { data } = await createCommand({ variables: { input }, refetchQueries: [{ query: GET_COMMAND_LIST }] })
      if (data.createCommand == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    } else {
      const { data } = await editCommand({ variables: { id, input }, refetchQueries: [{ query: GET_COMMAND_LIST }] })
      if (data.editCommand == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    }

    return result
  }

  const handleCloseModal = () => {
    setIsModalShow(false)
  }

  return (
    <Section>

      <Container>
        <Title align="center">Command Rating</Title>

        <TableWrapper>
          {commands &&
            <Table>
              <thead>
                <Th>Team name</Th>
                <Th>City</Th>
                <Th>Score</Th>
                {viewer.isLoggedIn && <Th></Th>}
              </thead>
              <tbody>
                {commands.map(cm => (
                  <tr key={cm._id}>
                    <td>{cm.name}</td>
                    <td>{cm.city}</td>
                    <td>{cm.score}</td>
                    {viewer.isLoggedIn &&
                      <td>
                        <Button onClick={() => handleEdit(cm)}>Edit</Button>
                        <Button onClick={() => handleDelete(cm._id)}>Delete</Button>
                      </td>
                    }
                  </tr>
                ))}
              </tbody>
            </Table>
          }
        </TableWrapper>

        {viewer.isLoggedIn && 
          <Row style={{ justifyContent: "center" }}>
            <Button onClick={() => handleCreate()}>Add</Button>
          </Row>
        }

        {isModalShow && <EditCommandForm command={cm} onSave={handleSave} onClose={handleCloseModal} />}

      </Container>
    </Section>
  )
}

export default CommandRatingPage
