import React from 'react'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { Button, Section, Container } from '../components/Global'
import Spinner from '../components/Spinner'

const GET_VIEWER = gql`
  query GetViewer {
    viewer {
      email
      isLoggedIn
    }
  }
`

function ProfilePage(props) {
  const { client, data, loading } = useQuery(GET_VIEWER)

  if (loading) return <Spinner />

  if (!data.viewer.isLoggedIn) {
    props.history.push("/login")
  }

  const handleOnClick = async () => {
    localStorage.removeItem('token')
    props.history.push("/login")
    await client.resetStore()
  }

  return (
    <Section>
      <Container>
        <div>Welcome, {data.viewer.email}!</div>
        <Button onClick={handleOnClick}>Exit</Button>
      </Container>
    </Section>
  )
}

export default ProfilePage