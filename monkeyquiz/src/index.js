import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { ApolloClient } from 'apollo-client'
import { ApolloProvider } from '@apollo/react-hooks'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import IndexPage from './pages/IndexPage'
import AboutPage from './pages/AboutPage'
import QuizPage from './pages/QuizPage'
import ProfilePage from './pages/ProfilePage'
import CommandRatingPage from './pages/CommandRatingPage'
import LoginPage from './pages/LoginPage'
import Header from './components/Header'
import Footer from './components/Footer'
import { GlobalStyle } from './components/Global'

const cache = new InMemoryCache()
const httpLink = createHttpLink({
  uri: 'http://localhost:4000/api/graphql'
})

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem("token")

  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ""
    }
  }
})

const client = new ApolloClient({
  cache,
  link: authLink.concat(httpLink),
  connectToDevTools: true
})

function App() {
  return (
    <BrowserRouter>
      <GlobalStyle />
      <Header />
      <Switch>
        <Route exact path='/' component={IndexPage} />
        <Route path="/quiz/:id" component={QuizPage} />
        <Route path='/profile' component={ProfilePage} />
        <Route path='/about' component={AboutPage} />
        <Route path='/rating' component={CommandRatingPage} />
        <Route path='/login' component={LoginPage} />
      </Switch>
      <Footer />
    </BrowserRouter>
  )
}

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root')
)
