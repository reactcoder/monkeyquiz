import React from 'react'
import styled from 'styled-components'


import CalendarImg from '../static/dsfs.png'
import SearchImg from '../static/search.png'

const Filter = styled.div`
  display: flex;  
  @media (max-width: 640px) {
    flex-direction: column;             
  }
`
const Calendar = styled.div `
  border: 2px solid #fff;
  border-radius: 8px;
  width: 240px;
  height: 44px;
  font-size: 15px;
  font-weight: 700;
  color: #fff;
  position: relative;
  text-align: center;
  padding-left: 7px;
  line-height: 2.6;
  cursor: pointer;
  @media (max-width: 640px) {
    margin: 0 auto;           
  }
`
const CalendarIcon = styled.img `
  width: 16px;
  height: 18px;
  top: 11px;
  left: 80px;
  position: absolute;
  z - index: 1;
`
const FilterSort = styled.div `
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  margin-left: 32px;
  @media (max-width: 640px) {
    flex-wrap: wrap;     
    justify-content: center;   
    margin-left: 0;   
    margin-top: 20px; 
  }
`

const FilterSortSpan = styled.span `
  display: flex;
  color: #fff;
  font-size: 16px;
  margin-right: 10px;
  @media (max-width: 640px) {
    margin-bottom: 10px;            
  }
`

const FilterSelect = styled.select `
  border: 2px solid #fff;
  color: #fff;
  font-size: 16px;
  border-radius: 8px;
  background-color: transparent;
  width: 240px;
  height: 44px;
  padding-left: 10px;
  cursor: pointer;
  &:focus {
    outline: none;
  }
`

const FilterOption = styled.option `
  color: #ff9652;
  cursor: pointer;
`

const FilterSearch = styled.div `
  margin-left: 32px;
  height: 100%;
  @media (max-width: 640px) {
    display: flex;
    justify-content: center;  
    margin-left: 0;     
    margin-top: 20px;  
  }
`

const FilterSearchGroup = styled.div `
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
` 

const FilterSearchInput = styled.input `
  border: 2px solid #fff;
  color: #fff;
  font-size: 16px;
  border-radius: 8px;
  background-color: transparent;
  padding: 13px;
  width: 240px;
  height: 44px;
  padding-left: 10px;
  &::placeholder {
    color: #fff;
  }
  &:focus {
    outline: none;
  }
`

const FilterSearchIcon = styled.img `
  position: absolute;
  right: 10px;
  width: 15px;
  height: 16px;
`


export default () => (
   <Filter>
    <Calendar><CalendarIcon src={CalendarImg} />Даты</Calendar>
      <FilterSort>
        <FilterSortSpan>Сортировка:</FilterSortSpan>
        <FilterSelect>
          <FilterOption data-display="По рейтингу">По рейтингу</FilterOption>
          <FilterOption value="1">По цене: сначала дешевле</FilterOption>
          <FilterOption value="2">По цене: сначала дороже</FilterOption>
          <FilterOption value="3">По популярности</FilterOption>
        </FilterSelect>
      </FilterSort>
    <FilterSearch>
      <form>
        <FilterSearchGroup>
          <FilterSearchInput type="search" placeholder="Поиск в описании" />
          <FilterSearchIcon src={SearchImg}/>
        </FilterSearchGroup>
      </form>
    </FilterSearch>
   </Filter>
)


