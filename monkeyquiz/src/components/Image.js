import React from 'react'
import styled from 'styled-components'

import { ReactComponent as Play } from '../static/play.svg'
import { ReactComponent as Comment } from '../static/comment.svg'
import { ReactComponent as Like } from '../static/like.svg'

const ImgContainer = styled.div`
  position: relative;
  flex-basis: 100%;
  flex-basis: calc(33.333% - 20px);
  margin: 10px;
  cursor: pointer;
  transition: .5s all ease-in;
`

const Img = styled.img`
  cursor: pointer;
  width: 100%;
`

const ImgMeta = styled.div`
  display: none;
  align-items: center;
  justify-content: center;
  position: absolute;
  background-color: rgba(0,0,0,.5);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  ${ImgContainer}:hover & {
    display: flex !important;
  }
`

const ImgIcons = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  margin-right: 20px;

  svg {
    margin-right: 10px;
    width: 24px;
    height: 24px;
    fill: #fff;
  }
`

function Image({ image }) {
  return (
    <ImgContainer>
      <Img src={image.source} />
      <ImgMeta>
        <ImgIcons>
          {image.isVideo ? <Play /> : <Like />} {image.likes}
        </ImgIcons>
        <ImgIcons>
          <Comment /> {image.comments}
        </ImgIcons>
      </ImgMeta>
    </ImgContainer>
  )
}

export default Image