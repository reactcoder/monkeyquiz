import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

const GET_VIEWER = gql`
  query GetViewer {
    viewer {
      email
      isLoggedIn
    }
  }
`

const Menu = styled.ul `
  display: flex;
  list-style-type: none;
  @media (max-width: 640px) {
    display: none;
  }
`
const MenuItem = styled.li `
  margin-left: 50px;
`
const NavLink = styled(Link) ` 
  text-decoration: none;
  font-size: 15px;
  font-weight: 700;
  color: rgba(255,150,82,1);
`
const DropdownBtn = styled.button `
  display: none;
  border: none;
  background-color: transparent;
  margin-top: 10px;
  font-size: 22px;
  color: rgba(255,150,82,1);
  @media (max-width: 640px) {
    display: block;
  }
`

const Dropdown = styled.div `
  @media(max-width: 640px) {
    position: absolute;
    top: 75px;
    left: 0;
    width: 100%;
    background: #fff;
    color: #333;
    z-index: 90;
    border-top: 2px solid #ff6f7f;
    border-bottom: 2px solid #ff6f7f;
  }
`
const MobMenu = styled.ul `
  list-style-type: none;
  padding-bottom: 20px;
  @media (max-width: 640px) {
    list-style-type: none;
    padding-bottom: 20px;
  }
`

const MobMenuItem = styled.li `
  line-height: 2;
  cursor: pointer;
`

const MobMenuLink = styled(Link) `  
  font-size: 15px;
  font-weight: 700;
  color: rgba(255,150,82,1);
  text-decoration: none;
`

export default class extends React.Component {
  state = {
    isMenuOpen: false
  }

  onMenuToggle = () => {
    this.setState(({ isMenuOpen }) => {
      return {
        isMenuOpen: !isMenuOpen
      }
    })
  }

  render() {
    const { isMenuOpen } = this.state
    const icon = isMenuOpen ? <i className="fas fa-times"></i> : <i className="fas fa-bars"></i>
    return (
      <div>   
        <Menu>
          <MenuItem>
            <NavLink to={`/`}>Quizes</NavLink>
          </MenuItem>

          <MenuItem>
            <NavLink to={`/about`}>About</NavLink>
          </MenuItem>

          <MenuItem>
            <NavLink to={`/rating`}>Command Rating</NavLink>
          </MenuItem>

          <MenuItem>
            <Query query={GET_VIEWER}>
              {({ data }) => {
                if (data && data.viewer.isLoggedIn) {
                  return <NavLink to="/profile">Profile</NavLink>
                } else {
                  return <NavLink to="/login">Login</NavLink>
                }
              }}
            </Query>
          </MenuItem>
        </Menu> 
                
        <DropdownBtn onClick={this.onMenuToggle}>{icon}</DropdownBtn>
        {
          isMenuOpen &&
          <Dropdown>
            <MobMenu>
              <MobMenuItem>
                <MobMenuLink to="/">Quizes</MobMenuLink>
              </MobMenuItem>

              <MobMenuItem>
                <MobMenuLink to="/about">About</MobMenuLink>
              </MobMenuItem>

              <MobMenuItem>
                <MobMenuLink to="/rating">Command Rating</MobMenuLink>
              </MobMenuItem>

              <MobMenuItem>
                <Query query={GET_VIEWER}>
                  {({ data }) => {
                    if (data && data.viewer.isLoggedIn) {
                      return <MobMenuLink to="/profile">Profile</MobMenuLink>
                    } else {
                      return <MobMenuLink to="/login">Login</MobMenuLink>
                    }
                  }}
                </Query>
                
              </MobMenuItem>
            </MobMenu>
          </Dropdown>
        }
      </div>
    )
  }
}