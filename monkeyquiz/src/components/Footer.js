import React from 'react'
import styled from 'styled-components'
import { Input, Button, Container, Row, Section } from './Global'

const Footer = styled.footer`  
  padding-top: 65px;
  padding-bottom: 80px;
  background: linear-gradient(to right,rgba(20,20,20,.8) 0%,rgba(20,20,20,1) 100%);
  color: #fff;
  @media (max-width: 640px) {
    padding-top: 40px;
    padding-bottom: 40px;
  }

  ${Input} {
    margin-right: 10px;
  }
`
const FooterContainer = styled.div`
  max-width: 1200px;
  padding: 0 15px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  @media (max-width: 640px) {
    flex-direction: column;
  }
`

const FooterSubscribe = styled.div `  
  display: flex;
  flex: 1 1 0;
  flex-direction: column;
`

const FooterHead = styled.h3 `
  font-size: 28px;
  color: #fff;
  margin-bottom: 30px;
  @media (max-width: 640px) {
    font-size: 24px;
  }
`

const FooterInfo = styled.div `
  display: flex;
  flex-direction: column;
  flex: 1 1 0;
  padding-right: 20px;
  @media (max-width: 640px) {
    margin-top: 30px;
  }
`
const FooterInfoDescription = styled.p `
  font-size: 15px;
  line-height: 24px;
  color: #fff;
`
const FooterInfoName = styled.span `
  display: flex;
  text-transform: uppercase;
  font-size: 20px;
  margin-bottom: 20px;
`
const FooterInfoEmail = styled.a `
  display: flex;
  color: #fff;
  text-decoration: none;
`

const FooterCopy = styled.div `
  margin-top: 30px;
  font-size: 15px;
  color: #fff;
`

const FooterForm = styled.form`
  display: flex;
`

const FooterRow = styled(Row)`
  @media (max-width: 640px) {
    flex-direction: column;
    align-items: flex-start;
  }
`


export default () => (
  <Footer>
    <Container>
      <FooterRow>
        <FooterInfo>
          <FooterInfoDescription>
            <FooterInfoName>Awesome Quizes</FooterInfoName>
            Am Grasbrookpark 1f, 20457 Hamburg
            <FooterInfoEmail href="#">natalia@monkeyquiz.de</FooterInfoEmail>
            <FooterCopy>&copy; 2019 by Natalia Muntyan</FooterCopy>
          </FooterInfoDescription>
        </FooterInfo>
        <FooterSubscribe>
          <FooterHead>Subscribe for Us</FooterHead>
          <FooterForm>
            <Input type="phone" placeholder="Your phone number" />
            <Button>subscribe</Button>
          </FooterForm>
        </FooterSubscribe>
      </FooterRow>
    </Container>
  </Footer>
)