import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Clock, Address, Price, Icon, Button, RoundButton } from './Global'
import { getTime, convertCurrency } from '../utils/utils'

const QuizBlockWrapper = styled.div`
  text-decoration: none;
  background-color: #fff;
  padding-bottom: 30px;
  box-shadow: 1px 1px 3px rgba(0,0,0,.2);
  position: relative;
  margin: 10px;

  width: calc((100% - 60px) / 3);

  @media (max-width: 796px) {
    width: calc((100% - 40px) / 2);
  }

  @media (max-width: 640px) {
    width: 100%;
    margin: 0 0 10px 0;
  }
`
const QuizImg = styled.img `
  max-width: 100%;
  width: 100%;
  height: auto;
`
const QuizHead = styled.h3 `
  text-decoration: none;
  font-size: 24px;
  margin: 0;
  color: rgba(255,150,82,1);
  @media (max-width: 640px) {
    font-size: 18px;
  }
`

const QuizDescription = styled.div `
  padding: 15px;

  &:nth-child(1) {
    border-bottom: 1px solid #ccc;
  }
`

const QuizData = styled.span `
  font-size: 15px;
`

const QuizAdress = styled.p `
  text-align: left;
  padding: 0;
  margin: 0;
`

const Line = styled.hr`
  width: 80%;
  margin: 0 auto;
  height: 2px;
  background: linear-gradient(to right, rgba(255,150,82,.8) 0%, rgba(255,150,82,1) 100%);
  border: 1px;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #000;
`

const QuizDeleteButton = styled(RoundButton)`
  position: absolute;
  background-color: #fff;
  top: -12px;
  right: -12px;
`

const QuizEditButton = styled(RoundButton)`
  position: absolute;
  background-color: #fff;
  top: -12px;
  right: 12px;
`

function QuizBlock({ quiz, onEdit, onDelete }) {
  return (
    <QuizBlockWrapper>    
      <QuizImg src={quiz.photo}/>
      
      <QuizDescription>
        <Row small>
          <QuizData>{new Date(quiz.date).toLocaleDateString()}</QuizData>
        </Row>

        <Row small>
          <QuizHead>{quiz.title}</QuizHead>
        </Row>

        <Row small>
          <Icon>
            <Address />
          </Icon>
          <QuizAdress>{quiz.city}</QuizAdress>
        </Row>
      </QuizDescription>

      <Row small>
        <Line />
      </Row>

      <QuizDescription>
        <Row small>
          <Icon>
            <Clock />
          </Icon>
          <QuizData>{quiz.time ? quiz.time : ""}</QuizData>
        </Row>

        <Row small>
          <Icon>
            <Address />
          </Icon>
          <QuizAdress>{quiz.location}</QuizAdress>
        </Row>

        <Row small>
          <Icon>
            <Price />
          </Icon>
          <QuizAdress>{convertCurrency(quiz.currency, quiz.cost)}</QuizAdress>
        </Row>

        <Row>
          <StyledLink to={`/quiz/${quiz._id}`}>
            <Button>Details</Button>
          </StyledLink>
        </Row>
      </QuizDescription>

      {onDelete && <QuizDeleteButton onClick={() => onDelete(quiz._id)}>X</QuizDeleteButton>}
      {onEdit && <QuizEditButton onClick={() => onEdit(quiz)}>E</QuizEditButton>}
    </QuizBlockWrapper>
  )
}

export default QuizBlock