import React, { useState } from 'react'
import styled from 'styled-components'
import {
  Input,
  Button,
  Range,
  Label,
  Checkbox,
  Row,
  Icon,
  Clock,
  Address,
  Price,
  Modal,
  ModalWrapper,
  ModalCloseButton
} from './Global'
import { convertCurrency } from '../utils/utils'

const Span = styled.span``

const Title = styled.h1`
  margin-top: 0;
  text-align: center;
  font-size: 25px;
`

const NumOfParticipants = styled.span`
  line-height: .9;
  font-size: 14px;
`

const CountOfParticipants = styled.span`
  margin-left: 10px;
`

function RegistrationForm({ quiz, onClose }) {
  const [count, setCount] = useState(2)

  return (
    <Modal>
      <ModalWrapper>
        <Title>Registration Form</Title>
        
        <Row>
          <Icon>
            <Clock />
          </Icon>
          <Span><strong>Time:</strong> {new Date(quiz.date).toLocaleString()}</Span>
        </Row>

        <Row>
          <Icon>
            <Address />
          </Icon>
          <Span><strong>Address:</strong> {quiz.location}</Span>
        </Row>

        <Row>
          <Icon>
            <Price />
          </Icon>
          <Span><strong>Price:</strong> {convertCurrency(quiz.currency, quiz.cost)}</Span>
        </Row>

        <Row small>
          <Input type="text" placeholder="Name (*)" />
        </Row>

        <Row small>
          <Input type="phone" placeholder="Telephone (*)" />
        </Row>

        <Row small>
          <Input type="email" placeholder="Email" />
        </Row>

        <Row small>
          <Input type="text" placeholder="Team Name" />
        </Row>

        <Row center>
          <NumOfParticipants>Number of participants</NumOfParticipants>
          <Range type="range" min={2} max={10} step={1} value={count} defaultValue={2}
            onChange={ (e) => setCount(e.target.value) } />
          <CountOfParticipants>{count}</CountOfParticipants>
        </Row>

        <Row>
          <Label>
            <Checkbox type="checkbox" />
            I need a team
          </Label>
        </Row>

        <Row center>
          <Button>Register</Button>
        </Row>

        <ModalCloseButton onClick={onClose}>x</ModalCloseButton>

      </ModalWrapper>
    </Modal>
  )
}

export default RegistrationForm