import React from 'react'
import styled from 'styled-components'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

const Button = styled.button`
  margin-top: 30px;
  font-size: 15px;
  font-weight: 700;
  color: #fff;
  background-color: #fe8163;
  border: none;
  text-transform: uppercase;
  padding: 1rem 2rem;
  border-radius: 10px;
  cursor: pointer;
`

const BUY = gql`
  mutation Buy($id: ID!) {
    buy(id: $id) {
      _id
      title
      city
    }
  }
`

export default ({ id, children }) => {
  const [buy] = useMutation(BUY, { variables: { id } })

  const clickHandler = async (e) => {
    const { data } = await buy()
    alert(`Вы купили билет на квиз ${data.buy.title}`)
  }

  return (
    <Button onClick={clickHandler}>{children}</Button>
  )
}