import { MongoClient } from "mongodb"

const DB_URL  = "mongodb://admin:s012345678@ds331198.mlab.com:31198/monkeyquiz"
const DB_NAME = "monkeyquiz"
const DB_OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

export default (req, res, next) => {
  const client = new MongoClient(DB_URL, DB_OPTIONS)

  if (!client.isConnected()) {
    return client.connect().then(() => {
      req.db = client.db(DB_NAME)
      return next()
    })
  }
  req.db = client.db(DB_NAME)
  console.log(`>> Подключились к БД ${req.db}`)
  next()
}
