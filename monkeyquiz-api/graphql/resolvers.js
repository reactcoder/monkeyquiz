import jwt from "jsonwebtoken"
import bcrypt from 'bcryptjs'
import { ObjectId } from 'mongodb'

const TOKEN_SECRET = "super secret"

const resolvers = {
  Query: {
    quizes: async (parent, args, { db }) => {
      let s = null
      let l = args.limit ? args.limit : 0

      if (args.sortby) {
        if (args.sortby == "CITY")        s = { city: 1 }
        if (args.sortby == "GAME_TYPE")   s = { gameType: 1 }
        if (args.sortby == "DATE")        s = { date: -1 }
      }

      return await db
        .collection("quizes")
        .find()
        .sort(s)
        .limit(l)
        .toArray()
    },

    quiz: async (parent, { id }, { db }) => {
      return await db
        .collection('quizes')
        .findOne({ _id: +id })
    },

    commands: async (parent, args, { db }) => {
      return await db
        .collection('commands')
        .find()
        .sort({ score: -1 })
        .toArray()
    },

    command: async (parent, { id }, { db }) => {
      return await db
        .collection('commands')
        .findOne({ _id: ObjectId(id) })
    },

    gameTypes: async (parent, args, { db }) => {
      return await db
        .collection('quizes')
        .distinct('gameType')
    },

    cities: async (parent, args, { db }) => {
      return await db
        .collection('quizes')
        .distinct('city')
    },

    viewer: async (parent, args, { isAuth, userId, db }) => {
      if (!isAuth) {
        return { isLoggedIn: false }
      }

      const user = await db
        .collection('users')
        .findOne({ _id: ObjectId(userId) })

      return {
        ...user,
        isLoggedIn: true
      }
    }
  },

  Mutation: {
    buy: async (parent, { id }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const quiz = await db
        .collection('quizes')
        .findOne({ _id: +id })

      // let message = {
      //   from: 'vmp@live.ru',
      //   to: 'vmp@live.ru',
      //   subject: 'Письмо',
      //   text: JSON.stringify(qz),
      //   // html: ''
      // }

      // transport.sendMail(message, (err) => {
      //   if (err)
      //     console.log(err)
      //   console.log('Message sended succefully...')
      // })

      return quiz
    },

    createQuiz: async (parent, { input }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const ids = await db
        .collection('quizes')
        .distinct("_id")
        
      const sortedIds = ids.sort((a, b) => b - a)

      const result = await db
        .collection('quizes')
        .insertOne({ _id: sortedIds[0] + 1, ...input })
        
      if (result && result.result.ok) {
        return "SUCCESS"
      }
    
      return "ERROR"
    },

    editQuiz: async (parent, { id, input }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const quiz = await db
        .collection('quizes')
        .findOneAndUpdate({ _id: +id }, { $set: { ...input } })
        
      if (quiz && quiz.ok) {
        return "SUCCESS"
      }
        
      return "ERROR"
    },

    deleteQuiz: async (parent, { id }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      let quiz = await db
        .collection('quizes')
        .deleteOne({ _id: +id })
        
          
      if (quiz && quiz.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    },

    editCommand: async (parent, { id, input }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const command = await db
        .collection('commands')
        .findOneAndUpdate({ _id: ObjectId(id) }, { $set: { ...input } })
        
      if (command && command.ok) {
        return "SUCCESS"
      }
      
      return "ERROR"
    },

    createCommand: async (parent, { input }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const command = await db
        .collection('commands')
        .insertOne({ ...input })
          
      if (command && command.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    },

    deleteCommand: async (parent, { id }, { isAuth, db }) => {
      if (!isAuth) {
        throw new Error("Пользователь неавторизован!")
      }

      const command = await db
        .collection('commands')
        .deleteOne({ _id: ObjectId(id) })
          
      if (command && command.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    },

    userLogin: async (parent, { email, password }, { db }) => {
      const user = await db
        .collection('users')
        .findOne({ email })

      if (!user) {
        throw new Error("Пользователь не найден в системе!")
      }

      const isEqual = bcrypt.compare(password, user.password)
      if (!isEqual) {
        throw new Error("Пароль не верен. Попробуйте еще раз!")
      }
      
      const payload = { userId: user._id }
      const token = jwt.sign(payload, TOKEN_SECRET, {
        expiresIn: "1h"
      })
        
      return { token }
    },

    userCreate: async (parent, { email, password }, { db }) => {
      const user = await db
        .collection("users")
        .findOne({ email })

      if (user) {
        throw new Error("Пользователь с таким email уже существует!")
      }

      const hashedPassword = await bcrypt.hash(password, 12)

      const result = await db
        .collection("users")
        .insertOne({ email, password: hashedPassword })

      if (result && result.result.ok) {
        return "SUCCESS"
      }

      return "ERROR"
    }
  }
}

export default resolvers