import { MongoClient } from 'mongodb'

const DB_URL  = "mongodb://admin:s012345678@ds331198.mlab.com:31198/monkeyquiz"
const DB_NAME = "monkeyquiz"
const DB_OPTIONS = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}

export const connectDB = async () => {
  const client = new MongoClient(DB_URL, DB_OPTIONS)

  if (!client.isConnected()) {
    await client.connect()
  }

  return client.db(DB_NAME)
}