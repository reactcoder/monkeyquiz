import jwt from 'jsonwebtoken'

const TOKEN_SECRET = "super secret"

export const auth = async (req) => {
  const authHeader = req.headers.authorization || req.headers["Authorization"]
  if (!authHeader) {
    return { isAuth: false }
  }
  
  const token = authHeader.split(" ")[1]
  if (!token || token === "") {
    return { isAuth: false }
  }
  
  let decodedToken
  try {
    decodedToken = await jwt.verify(token, TOKEN_SECRET)
  } catch (error) {
    return { isAuth: false }
  }

  return { isAuth: true, userId: decodedToken.userId }  
}