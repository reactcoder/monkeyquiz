import { ApolloServer } from 'apollo-server-micro'
import microCors from 'micro-cors'
import typeDefs from '../../graphql/typeDefs'
import resolvers from '../../graphql/resolvers'
import { connectDB } from '../../lib/db'
import { auth } from '../../lib/auth'

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    
    const db = await connectDB()
    const { isAuth, userId } = await auth(req)

    return {
      db,
      isAuth,
      userId
    }
  },
  introspection: true,
  playground: true
})

export const config = {
  api: {
    bodyParser: false
  }
}

const cors = microCors()
const handler = apolloServer.createHandler({ path: '/api/graphql' })
 
export default cors(handler)