import express from 'express'
import cors from 'cors'
import { ApolloServer } from 'apollo-server-express'
import typeDefs from '../../graphql/typeDefs'
import resolvers from '../../graphql/resolvers'
import { connectDB } from '../../lib/db'
import { auth } from '../../lib/auth'

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const db = await connectDB()
    const { isAuth, userId } = await auth(req)

    return {
      db,
      isAuth,
      userId
    }

  },
  introspection: true,
  playground: true
})

const app = express()

app.use(cors())
app.options((req, res, next) => {
  res.status(200).send("ok")
})

server.applyMiddleware({ app, path: "/api/graphql" })

export const config = {
  api: {
    bodyParser: false
  }
}

export default app