import Link from 'next/link'

export default () => (
  <div>
    <h1>GraphQL API</h1>
    <p>
      <Link href="/api/graphql">
        <a>graphql</a>
      </Link>
    </p>
  </div>
)