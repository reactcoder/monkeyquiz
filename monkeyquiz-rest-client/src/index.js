import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import IndexPage from './pages/IndexPage'
import AboutPage from './pages/AboutPage'
import QuizPage from './pages/QuizPage'
import ProfilePage from './pages/ProfilePage'
import CommandRatingPage from './pages/CommandRatingPage'
import LoginPage from './pages/LoginPage'
import Header from './components/Header'
import Footer from './components/Footer'
import { GlobalStyle } from './components/Global'
import { ApiContext } from './lib/ApiContext'
import ApiClient from './lib/ApiClient'

const client = new ApiClient("http://localhost:4000/api")

function App() {
  return (
    <BrowserRouter>
        <GlobalStyle />
        <Header />
        <Switch>
          <Route exact path='/' component={IndexPage} />
          <Route path="/quiz/:id" component={QuizPage} />
          <Route path='/profile' component={ProfilePage} />
          <Route path='/about' component={AboutPage} />
          <Route path='/rating' component={CommandRatingPage} />
          <Route path='/login' component={LoginPage} />
        </Switch>
        <Footer />
    </BrowserRouter>
  )
}

ReactDOM.render(
  <ApiContext.Provider value={client}>
    <App />
  </ApiContext.Provider>,
  document.getElementById('root')
)
