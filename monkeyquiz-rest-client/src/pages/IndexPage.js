import React, { useState, useEffect, useContext } from 'react'
import QuizBlock from '../components/QuizBlock'
import EditQuizForm from '../components/EditQuizForm'
import {
  Container,
  QuizWrapper,
  QuizHead,
  QuizContent,
  AddBlock,
  AddBlockInner
} from '../components/Global'
import { ApiContext } from '../lib/ApiContext'
import Spinner from '../components/Spinner'

function IndexPage() {
  const api = useContext(ApiContext)
  const [quizes, setQuizes] = useState([])
  const [viewer, setViewer] = useState({ isLoggedIn: false })
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [isModalShow, setIsModalShow] = useState(false)
  const [quiz, setQuiz] = useState(null)
  const [updated, setUpdated] = useState(true)

  useEffect(() => {
    async function fetchData() {
      const quizes = await api.quizes()
      const viewer = await api.viewer()

      if (quizes.err) {
        setError({ message: quizes.err })
      } else {
        setQuizes(quizes)
        setViewer(viewer)
      }
    }
    setLoading(true)
    fetchData()
    setLoading(false)
    setUpdated(false)
  }, [updated])

  const handleSave = async (id, input) => {
    let result

    if (!id) {
      const data = await api.createQuiz(input)

      console.log(data)

      if (data.result == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    } else {
      const data = await api.editQuiz(id, input)

      if (data.result == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    }

    setUpdated(true)

    return result
  }

  const handleEdit = async (qz) => {
    setQuiz(qz)
    setIsModalShow(true)
  }

  const handleCreate = async () => {
    setQuiz(null)
    setIsModalShow(true)
  }

  const handleDelete = async (id) => {
    await api.deleteQuiz(id)
    setUpdated(true)
  }

  if (loading) return <Spinner />

  return (
    <QuizWrapper>
      <Container>
        <QuizHead>All Quizes</QuizHead>
        <QuizContent>
          { quizes.map(q =>
            <QuizBlock
              key={q._id}
              quiz={q}
              onEdit={viewer.isLoggedIn ? handleEdit : null}
              onDelete={viewer.isLoggedIn ? handleDelete : null}
            />)
          }
          {viewer.isLoggedIn &&
            <AddBlock onClick={handleCreate}>
              <AddBlockInner>+</AddBlockInner>
            </AddBlock>
          }
        </QuizContent>
      </Container>

      {isModalShow && <EditQuizForm quiz={quiz} onSave={handleSave} onClose={() => setIsModalShow(false)} />}
      {error && <div>{error.message}</div>}
    </QuizWrapper>
  )
}

export default IndexPage