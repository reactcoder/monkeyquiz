import React, { useContext, useEffect, useState } from 'react'
import { Button, Section, Container } from '../components/Global'
import { ApiContext } from '../lib/ApiContext'

function ProfilePage(props) {
  const api = useContext(ApiContext)
  const [email, setEmail] = useState(null)

  useEffect(() => {
    async function fetchData() {
      const { isLoggedIn, email } = await api.viewer()

      if (!isLoggedIn) {
        props.history.push('/login')
      }

      setEmail(email)
    }
    fetchData()
  }, [])

  const handleOnClick = async () => {
    localStorage.removeItem('token')
    props.history.push("/login")
  }

  return (
    <Section>
      <Container>
        <div>Welcome, {email}!</div>
        <Button onClick={handleOnClick}>Exit</Button>
      </Container>
    </Section>
  )
}

export default ProfilePage