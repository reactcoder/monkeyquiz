import React, { useState, useEffect, useContext } from 'react'
import { Section, Container, Title, Button, Row, TableWrapper, Table, Th } from '../components/Global'
import EditCommandForm from '../components/EditCommandForm'
import Spinner from '../components/Spinner'
import { ApiContext } from '../lib/ApiContext'

function CommandRatingPage(props) {
  const api = useContext(ApiContext)
  const [isModalShow, setIsModalShow] = useState(false)
  const [commands, setCommands] = useState([])
  const [viewer, setViewer] = useState({ isLoggedIn: false })
  const [cm, setCommand] = useState(null)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const [updated, setUpdated] = useState(true)

  useEffect(() => {
    async function fetchData() {
      const commands = await api.commands()
      const viewer = await api.viewer()

      if (commands.err) {
        setError({ message: commands.err })
      } else {
        setCommands(commands)
        setViewer(viewer)
      }
    }
    setLoading(true)
    fetchData()
    setLoading(false)
    setUpdated(false)
  }, [updated])

  const handleEdit = (cm) => {
    setCommand(cm)
    setIsModalShow(true)
  }

  const handleCreate = () => {
    setCommand(null)
    setIsModalShow(true)
  }

  const handleDelete = async (id) => {
    await api.deleteCommand(id)
    setUpdated(true)
  }

  const handleSave = async (id, input) => {
    let result

    if (!cm) {
      const data = await api.createCommand(input)
      if (data.result == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    } else {
      const data = await api.editCommand(id, input)
      if (data.result == "SUCCESS") {
        result = true
      } else {
        result = false
      }
    }

    setUpdated(true)

    return result
  }

  const handleCloseModal = () => {
    setIsModalShow(false)
  }

  console.log(commands)

  return loading ? <Spinner /> : (
    <Section>

      <Container>
        <Title align="center">Command Rating</Title>

        <TableWrapper>
          {commands &&
            <Table>
              <thead>
                <Th>Team name</Th>
                <Th>City</Th>
                <Th>Score</Th>
                {viewer.isLoggedIn && <Th></Th>}
              </thead>
              <tbody>
                {commands.map(cm => (
                  <tr key={cm._id}>
                    <td>{cm.name}</td>
                    <td>{cm.city}</td>
                    <td>{cm.score}</td>
                    {viewer.isLoggedIn &&
                      <td>
                        <Button onClick={() => handleEdit(cm)}>Edit</Button>
                        <Button onClick={() => handleDelete(cm._id)}>Delete</Button>
                      </td>
                    }
                  </tr>
                ))}
              </tbody>
            </Table>
          }
        </TableWrapper>

        {viewer.isLoggedIn && 
          <Row style={{ justifyContent: "center" }}>
            <Button onClick={() => handleCreate()}>Add</Button>
          </Row>
        }

        {isModalShow && <EditCommandForm command={cm} onSave={handleSave} onClose={handleCloseModal} />}

      </Container>
    </Section>
  )
}

export default CommandRatingPage
