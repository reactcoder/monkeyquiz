import React, { useState, useEffect, useContext } from 'react'
import RegisterForm from '../components/RegisterForm'
import {
  Button,
  Row,
  Icon,
  Clock,
  Compass,
  User,
  Price,
  QuizInfo,
  QuizHeader,
  QuizCity,
  QuizFooter,
  QuizLocation,
  QuizImg
} from '../components/Global'
import { convertCurrency } from '../utils/utils'
import Spinner from '../components/Spinner'
import { ApiContext } from '../lib/ApiContext'

function QuizPage({ match }) {
  const { id } = match.params
  const api = useContext(ApiContext)
  const [isShow, setIsShow] = useState(false)
  const [quiz, setQuiz] = useState(null)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    async function fetchData() {
      const quiz = await api.quiz(+id)

      if (quiz.err) {
        setError({ message: quiz.err })
      } else {
        setQuiz(quiz)
      }
    }
    setLoading(true)
    fetchData()
    setLoading(false)
  }, [])

  return loading ? <Spinner /> : (
    <QuizInfo>
      {isShow && <RegisterForm quiz={quiz} onClose={() => setIsShow(false)} />}

      {quiz &&
        <>
          <QuizImg src={quiz.photo}/>
          <QuizHeader>{quiz.title}</QuizHeader>
          <QuizCity>{quiz.city}</QuizCity>
          <Row>
            <Icon>
              <Clock />
            </Icon>
            <span><strong>Time:</strong> {new Date(quiz.date).toLocaleString()}</span>
          </Row>
          <Row small>
            <Icon>
              <Compass />
            </Icon>
            <QuizLocation><strong>Location:</strong> {quiz.location}</QuizLocation>
          </Row>
          <Row small>
            <Icon>
              <User />
            </Icon>
            <QuizLocation><strong>Game type:</strong> {quiz.gameType}</QuizLocation>
          </Row>
          <Row small>
            <Icon>
              <Price />
            </Icon>
            <QuizLocation><strong>Price:</strong> {convertCurrency(quiz.currency, quiz.cost)}</QuizLocation>
          </Row>
          <Row small>
            {quiz.fullDescription &&  <div>{quiz.fullDescription}</div>}
          </Row>

          <QuizFooter>
            <Button onClick={() => setIsShow(true)}>Registration</Button>
          </QuizFooter>
        </>}
    </QuizInfo>
  )
}

export default QuizPage