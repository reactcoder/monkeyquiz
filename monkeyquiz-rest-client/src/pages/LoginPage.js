import React, { useRef, useEffect, useState, useContext } from 'react'
import { Row, Input, Button, LoginForm, LoginLabel } from '../components/Global'
import { MiniSpinner } from '../components/Spinner'
import { ApiContext } from '../lib/ApiContext'

const LoginPage = (props) => {
  const emailRef = useRef()
  const passwRef = useRef()
  const api = useContext(ApiContext)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    async function fetchData() {
      setLoading(true)
      const { isLoggedIn } = await api.viewer()
      setLoading(false)

      if (isLoggedIn) {
        props.history.push('/profile')
      }
    }
    fetchData()
  }, [])

  const handleOnClick = async () => {
    const email = emailRef.current.value
    const password = passwRef.current.value

    try {
      setLoading(true)
      const { token, err } = await api.login(email, password)
      setLoading(false)

      if (err) {
        setError({ message: err })
      } else {
        localStorage.setItem('token', token)
        props.history.push(`/profile`)
      }

    } catch (error) {
      console.log(error)
      setLoading(false)
    }
  }

  return (
    <LoginForm>
      <Row>
        <h1>Login Form</h1>
      </Row>

      <Row small>
        <LoginLabel>
          Login:
          <Input type="text" placeholder="Login" ref={emailRef} />
        </LoginLabel>
      </Row>

      <Row small>
        <LoginLabel>
          Password:
          <Input type="text" placeholder="Password" ref={passwRef} />
        </LoginLabel>
      </Row>

      <Row style={{ justifyContent: "space-between", alignItems: "center" }}>
        <Button onClick={handleOnClick}>Login</Button>
        {loading && <MiniSpinner />}
      </Row>

      {error && <Row>{error.message}</Row>}
    </LoginForm>
  )
}

export default LoginPage