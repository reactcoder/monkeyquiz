import React from 'react'
import styled from 'styled-components'

const SpinnerWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
`

const LdsFacebook = styled.div`
  display: inline-block;
  position: relative;
  width: ${props => props.mini ? "32px" : "64px"};
  height: ${props => props.mini ? "32px" : "64px"};
  
  div {
    display: inline-block;
    position: absolute;
    left: ${props => props.mini ? "2px" : "6px"};
    width: ${props => props.mini ? "8px" : "13px"};
    background: #fff;
    background: linear-gradient(to right,rgba(255,150,82,.8) 0%,rgba(255,150,82,1) 100%);
    animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
  }

  div:nth-child(1) {
    left: ${props => props.mini ? "2px" : "6px"};
    animation-delay: -0.24s;
  }
  div:nth-child(2) {
    left: ${props => props.mini ? "12px" : "26px"};
    animation-delay: -0.12s;
  }
  div:nth-child(3) {
    left: ${props => props.mini ? "22px" : "45px"};
    animation-delay: 0;
  }

  @keyframes lds-facebook {
    0% {
      top: ${props => props.mini ? "0px" : "6px"};
      height: ${props => props.mini ? "32px" : "51px"};
    }
    50%, 100% {
      top: ${props => props.mini ? "8px" : "19px"};
      height: ${props => props.mini ? "16px" : "26px"};
    }
  }
`

function MiniSpinner() {
  return (
    <LdsFacebook mini>
      <div></div>
      <div></div>
      <div></div>
    </LdsFacebook>
  )
}

function Spinner() {
  return (
    <SpinnerWrapper>
      <LdsFacebook>
        <div></div>
        <div></div>
        <div></div>
      </LdsFacebook>
    </SpinnerWrapper>
  )
}

export default Spinner
export {
  MiniSpinner
}