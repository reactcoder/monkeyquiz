import React from 'react'
import styled from 'styled-components'
import Nav from './Nav'
import { Link } from 'react-router-dom'

const HeaderWrapper = styled.div`
  padding: 10px 0 15px 0;
  display: flex;
  position: relative;
  justify-content: space-between;
  align-items: center;
  max-width: 1200px;
  padding-left: 15px;
  padding-right: 15px;
  margin: 0 auto;
`

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #000;
  font-family: "Futura PT";
  font-size: 20px;
  font-weight: 900;
`

const Headerlogo = styled.div`
  text-decoration: none;
  display: flex;
  align-items: center;
  color: rgba(255,150,82,1);
  font-size: 24px;
`

function Header() {
  return (
    <HeaderWrapper>
      <StyledLink to="/">
        <Headerlogo>
          AwesomeQuizes
        </Headerlogo>
      </StyledLink>
      <Nav/>
    </HeaderWrapper>
  )
}

export default Header