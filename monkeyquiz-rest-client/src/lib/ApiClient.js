class ApiClient {
  constructor(apiUrl) {
    this.apiUrl = apiUrl
  }

  getResource = async (url, method, body) => {
    try {
      const token = localStorage.getItem('token')
      const ops = {
        method,
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify(body)
      }
      const response = await fetch(this.apiUrl + url, ops)
      return await response.json()
    } catch (error) {
      console.log(error)
    }
  }

  login = async (email, password) => {
    return await this.getResource('/login', "POST", { email, password })
  }

  viewer = async () => {
    return await this.getResource('/viewer', "GET")
  }

  quizes = async () => {
    return await this.getResource('/quizes', "GET")
  }

  quiz = async (id) => {
    return await this.getResource(`/quizes/${id}`, "GET")
  }

  createQuiz = async (input) => {
    return await this.getResource('/quizes', "POST", { ...input })
  }

  editQuiz = async (id, input) => {
    return await this.getResource(`/quizes/${id}`, "PUT", { ...input })
  }

  deleteQuiz = async (id) => {
    return await this.getResource(`/quizes/${id}`, "DELETE")
  }

  commands = async () => {
    return await this.getResource('/commands', "GET")
  }

  command = async (id) => {
    return await this.getResource(`/commands/${id}`, "GET")
  }

  createCommand = async (input) => {
    return await this.getResource(`/commands`, "POST", { ...input })
  }

  editCommand = async (id, input) => {
    return await this.getResource(`/commands/${id}`, "PUT", { ...input })
  }

  deleteCommand = async (id) => {
    return await this.getResource(`/commands/${id}`, "DELETE")
  }
}

export default ApiClient